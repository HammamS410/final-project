import { createBrowserRouter, RouterProvider } from "react-router-dom";
import "./App.css";
import ArticleBox from "./components/articleBox/articleBox";
import ArticleContainer from "./components/articleContainer/ArticleContainer";
import TableArticle from "./components/articleTable/TableArticle";
import CreateForm from "./components/createForm/CreateForm";
import EditForm from "./components/editForm/EditForm";
import HomePage from "./components/home/HomePage";
import LoginForm from "./components/loginForm/LoginForm";
import RegisterForm from "./components/registerForm/RegisterForm";

const router = createBrowserRouter([
  {
    path: "/",
    element: <HomePage />,
  },
  {
    path: "/home",
    element: <HomePage />,
  },
  {
    path: "/login",
    element: <LoginForm />,
  },
  {
    path: "/register",
    element: <RegisterForm />,
  },
  {
    path: "/product",
    element: <ArticleContainer />,
  },
  {
    path: "/setting",
    element: <TableArticle />,
  },
  {
    path: "/create",
    element: <CreateForm />,
  },
  {
    path: "/edit/:id",
    element: <EditForm />,
  },
  {
    path: "/productbox",
    element: <articleBox />,
  },
]);

function App() {
  return (
    <div>
      <RouterProvider router={router} />
    </div>
  );
}

export default App;
