import axios from "axios";
import React, { useEffect, useState } from "react";

function ArticleBox(props) {
  const [articles, setArticles] = useState([]);
  const [loading, setLoading] = useState(false);

  const fetchArticles = async () => {
    setLoading(true);
    try {
      const response = await axios.get("https://arhandev.xyz/public/api/final/products");
      console.log(response.data);
      setArticles(response.data.data);
    } catch (error) {
      console.log(error);
    } finally {
      setLoading(false);
    }
  };

  useEffect(() => {
    fetchArticles();
  }, []);

  return loading ? (
    <div className="text-3xl font-bold text-center">Loading...</div>
  ) : (
    <>
      <section>
        {articles.map((article) => {
          return (
            <div className="border-2 border-slate-300">
              <div>
                <img src={article.image_url} />
              </div>
              <div>
                <p>{article.nama}</p>
                <p>{article.harga_display}</p>
                <p>{article.harga_diskon_display}</p>
                <p>{article.harga_category}</p>
                <p>{article.harga_description}</p>
              </div>
            </div>
          );
        })}
      </section>
    </>
  );
}

export default ArticleBox;
