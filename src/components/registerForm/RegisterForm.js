import { Formik } from "formik";
import React, { useEffect, useState } from "react";
import * as Yup from "yup";
import axios from "axios";
import Navbar from "../Navbar";
import "./register.css";
import { useNavigate } from "react-router-dom";

const validationSchema = Yup.object({
  email: Yup.string().required(),
  name: Yup.string().required(),
  username: Yup.string().required(),
  password: Yup.string().required(),
  password_confirmation: Yup.string().required(),
});

function RegisterForm() {
  const navigate = useNavigate();
  const [loading, setLoading] = useState(false);
  const initialState = {
    email: "",
    name: "",
    username: "",
    password: "",
    password_confirmation: "",
  };

  const handleSubmit = async (values) => {
    setLoading(true);
    try {
      const response = await axios.post("https://arhandev.xyz/public/api/final/register", {
        email: values.email,
        name: values.name,
        username: values.username,
        password: values.password,
        password_confirmation: values.password_confirmation,
      });
      console.log(response.data.data);
      alert("Registrasi Akun Berhasil");
      navigate("/login");
    } catch (error) {
      console.log(error);
      alert(error.response.data.info);
    } finally {
      setLoading(false);
    }
  };

  return (
    <div>
      <Navbar />
      <div>
        <Formik initialValues={initialState} onSubmit={handleSubmit} validationSchema={validationSchema}>
          {(params) => {
            return (
              <form className="reg-form w-2/5 mx-auto flex flex-col gap-4 my-8 items-center border-2 border-slate-300 rounded-2xl px-4 py-4">
                <h1 className="text-2xl font-bold">Register</h1>

                <div className="w-full grid grid-cols-3 text-lg">
                  <label>Nama :</label>
                  <input type="text" name="name" onChange={params.handleChange} value={params.values.name} onBlur={params.handleBlur} placeholder=" Masukan nama lengkap" className="col-span-2 border-slate-300 border-2 rounded-md pl-2" />
                  <div></div>
                  <div>{params.touched.name && params.errors.name}</div>
                </div>

                <div className="w-full grid grid-cols-3 text-lg">
                  <label>Email :</label>
                  <input type="email" name="email" onChange={params.handleChange} value={params.values.email} onBlur={params.handleBlur} placeholder=" Masukan email" className="col-span-2 border-slate-300 border-2 rounded-md pl-2" />
                  <div></div>
                  <div>{params.touched.email && params.errors.email}</div>
                </div>

                <div className="w-full grid grid-cols-3 text-lg">
                  <label>Username :</label>
                  <input
                    type="text"
                    name="username"
                    onChange={params.handleChange}
                    value={params.values.username}
                    onBlur={params.handleBlur}
                    placeholder=" Masukan username"
                    className="col-span-2 border-slate-300 border-2 rounded-md pl-2"
                  />
                  <div></div>
                  <div>{params.touched.username && params.errors.username}</div>
                </div>

                <div className="w-full grid grid-cols-3 text-lg">
                  <label>Password :</label>
                  <input
                    type="password"
                    name="password"
                    onChange={params.handleChange}
                    value={params.values.password}
                    onBlur={params.handleBlur}
                    placeholder=" Masukan password"
                    className="col-span-2 border-slate-300 border-2 rounded-md pl-2"
                  />
                  <div></div>
                  <div>{params.touched.password && params.errors.password}</div>
                </div>

                <div className="w-full grid grid-cols-3 text-lg">
                  <label>Password confirmation :</label>
                  <input
                    type="password"
                    name="password_confirmation"
                    onChange={params.handleChange}
                    value={params.values.password_confirmation}
                    onBlur={params.handleBlur}
                    placeholder=" Masukan konfirmasi password"
                    className="col-span-2 border-slate-300 border-2 rounded-md pl-2"
                  />
                  <div></div>
                  <div>{params.touched.password_confirmation && params.errors.password_confirmation}</div>
                </div>

                <div className="w-full rounded-full border border-slate-500 text-center mx-auto bg-slate-700 text-white hover:bg-slate-400 py-2">
                  <button type="submit" disabled={loading} onClick={params.handleSubmit} className="item-center">
                    {loading === true ? "Loading..." : "Daftar"}
                  </button>
                </div>
              </form>
            );
          }}
        </Formik>
      </div>
    </div>
  );
}

export default RegisterForm;
