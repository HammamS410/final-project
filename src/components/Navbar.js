import React, { useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import { FaChevronUp, FaChevronDown } from "react-icons/fa";
import axios from "axios";

function Navbar() {
  const [isShow, setIsShow] = useState(false);
  const [loading, setLoading] = useState(false);
  const navigate = useNavigate();

  const onLogout = async () => {
    setLoading(true);
    try {
      const response = await axios.post(
        "https://arhandev.xyz/public/api/final/logout",
        {},
        {
          headers: { Authorization: `Bearer ${localStorage.getItem("token")}` },
        }
      );
      localStorage.removeItem("token");
      localStorage.removeItem("username");
      navigate("/login");
    } catch (error) {
      console.log(error);
      alert(error.response.data.info);
    } finally {
      setLoading(false);
    }
  };

  return (
    <div>
      <div
        className="flex justify-between bg-slate-700 text-lg text-white 
    py-2 px-5 items-center"
      >
        <ul className="flex gap-4 ">
          <Link to="/home">
            <button className="hover:text-slate-400  border-2 border-slate-400 px-4 py-1 rounded-full">Home</button>
          </Link>
          <Link to="/product">
            <button className="hover:text-slate-400  border-2 border-slate-400 px-4 py-1 rounded-full">Product</button>
          </Link>
        </ul>

        {localStorage.getItem("token") !== null ? (
          <div className="flex justify-center items-center">
            <div className="pr-5">
              {isShow ? (
                <FaChevronUp className="cursor-pointer border-2 border-slate-500 w-10 h-7 rounded-sm" onClick={() => setIsShow(!isShow)} />
              ) : (
                <FaChevronDown className="cursor-pointer border-2 border-slate-500 w-10 h-7 rounded-sm" onClick={() => setIsShow(!isShow)} />
              )}
            </div>

            <div className="flex items-center max-w-md">
              <ul className="flex flex-col mx-5 text-center gap-2">
                <li className="text-xl border-2 border-slate-500 rounded-lg px-8 py-1">{localStorage.getItem("username")}</li>
                {isShow && (
                  <ul>
                    <Link to="/setting">
                      <li className="text-xl border-2 border-slate-500 rounded-lg px-8 py-1">Setting Product</li>
                    </Link>
                  </ul>
                )}
              </ul>

              <ul>
                <button onClick={onLogout} className="hover:bg-slate-500 hover:text-white border px-5 py-1 rounded-full bg-slate-300 text-black">
                  Logout
                </button>
              </ul>
            </div>
          </div>
        ) : (
          <ul className="flex gap-2">
            <Link to="/login">
              <button className="border px-5 py-1.5 rounded-full">Login</button>
            </Link>
            <Link to="/register">
              <button className="border px-5 py-1.5 rounded-full">Register</button>
            </Link>
          </ul>
        )}
      </div>
    </div>
  );
}

export default Navbar;
