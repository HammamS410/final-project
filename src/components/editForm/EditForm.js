import { Formik } from "formik";
import React, { useEffect, useState } from "react";
import "./form.css";
import * as Yup from "yup";
import axios from "axios";
import Navbar from "../Navbar";
import { useNavigate, useParams } from "react-router-dom";

const validationSchema = Yup.object({
  nama: Yup.string().required(),
  harga: Yup.number().required(),
  stock: Yup.number().required(),
  deskripsi: Yup.string().required(),
  category: Yup.string().oneOf(["teknologi", "makanan", "miniman", "lainnya"]),
  image_url: Yup.string().required(),
});

const initialState = {
  nama: "",
  harga: 0,
  is_diskon: false,
  harga_diskon: 0,
  stock: 0,
  deskripsi: "",
  category: "teknologi",
  image_url: "",
};

function EditForm() {
  const [show, setShow] = useState(false);
  const [loading, setLoading] = useState(false);
  const params = useParams();
  const navigate = useNavigate();
  const [input, setInput] = useState(initialState);

  const fetchArticles = async () => {
    setLoading(true);
    try {
      const response = await axios.get(`https://arhandev.xyz/public/api/final/products`, { headers: { Authorization: `Bearer ${localStorage.getItem("token")}` } });
      setInput(
        {
          nama: response.data.data.nama,
          harga: response.data.data.harga,
          is_diskon: response.data.data.is_diskon,
          harga_diskon: response.data.data.harga_diskon,
          stock: response.data.data.stock,
          deskripsi: response.data.data.deskripsi,
          category: response.data.data.category,
          image_url: response.data.data.image_url,
        },
        { headers: { Authorization: `Bearer ${localStorage.getItem("token")}` } }
      );
    } catch (error) {
      console.log(error);
    } finally {
      setLoading(false);
    }
  };

  const handleSubmit = async (values) => {
    console.log(values);
    setLoading(true);
    try {
      const response = await axios.put(
        `https://arhandev.xyz/public/api/final/products/${params.id}`,
        {
          nama: values.nama,
          harga: values.harga,
          is_diskon: values.is_diskon,
          harga_diskon: values.harga_diskon,
          stock: values.stock,
          deskripsi: values.deskripsi,
          category: values.category,
          image_url: values.image_url,
        },
        { headers: { Authorization: `Bearer ${localStorage.getItem("token")}` } }
      );
      navigate("/setting");
    } catch (error) {
      // console.log(error.response.data.data);
      alert("Edit Gagal");
    } finally {
      setLoading(false);
    }
  };

  useEffect(() => {
    fetchArticles();
  }, []);

  return (
    <div>
      <Navbar />
      <div>
        <Formik initialValues={input} enableReinitialize onSubmit={handleSubmit} validationSchema={validationSchema}>
          {(params) => {
            return (
              <form className="form-format mx-auto flex flex-col gap-4 my-8 items-center border-2 border-slate-300 rounded-2xl px-4 py-4">
                <h1 className="text-2xl font-bold">Edit Product</h1>
                <div className="w-full grid grid-cols-3 text-lg">
                  <label>Nama :</label>
                  <input type="text" name="nama" onChange={params.handleChange} value={params.values.nama} onBlur={params.handleBlur} placeholder=" Masukan nama barang" className="col-span-2 border-slate-300 border-2 rounded-md pl-2" />
                  <div></div>
                  <div>{params.touched.nama && params.errors.nama}</div>
                </div>

                <div className="w-full grid grid-cols-3 text-lg">
                  <label>Harga :</label>
                  <input
                    type="number"
                    name="harga"
                    onChange={params.handleChange}
                    value={params.values.harga}
                    onBlur={params.handleBlur}
                    placeholder=" Masukan harga barang"
                    className="col-span-2 border-slate-300 border-2 rounded-md pl-2"
                  />
                  <div></div>
                  <div>{params.touched.harga && params.errors.harga}</div>
                </div>

                <div className="w-full grid grid-cols-2 text-lg justify-items-start">
                  <label className="mx-start">Aktifkan Diskon :</label>
                  <input
                    name="is_diskon"
                    className="justify-items-start"
                    type="checkbox"
                    onClick={(e) => {
                      setShow(!show);
                      if (e.target.checked) {
                        params.setFieldValue("is_diskon", 1);
                      } else {
                        params.setFieldValue("is_diskon", 0);
                      }
                    }}
                    onBlur={() => {
                      params.setFieldTouched("is_diskon");
                    }}
                    checked={params.values.is_diskon}
                  />
                </div>

                {show === true && (
                  <div className="w-full grid grid-cols-3 text-lg">
                    <label>Harga Diskon : </label>
                    <input
                      type="number"
                      name="harga_diskon"
                      onChange={params.handleChange}
                      value={params.values.harga_diskon}
                      onBlur={params.handleBlur}
                      placeholder="Masukan Harga Diskon"
                      className="col-span-2 border-slate-300 border-2 rounded-md pl-2 pr-2"
                    />
                    <div></div>
                    <div>{params.touched.harga_diskon && params.errors.harga_diskon}</div>
                  </div>
                )}

                <div className="w-full grid grid-cols-3 text-lg">
                  <label>stock :</label>
                  <input
                    type="number"
                    name="stock"
                    onChange={params.handleChange}
                    value={params.values.stock}
                    onBlur={params.handleBlur}
                    placeholder=" Masukan stock barang"
                    className="col-span-2 border-slate-300 border-2 rounded-md pl-2"
                  />
                  <div></div>
                  <div>{params.touched.stock && params.errors.stock}</div>
                </div>

                <div className="w-full grid grid-cols-3 text-lg">
                  <label>Deskripsi :</label>
                  <textarea
                    name="deskripsi"
                    onChange={params.handleChange}
                    value={params.values.deskripsi}
                    onBlur={params.handleBlur}
                    placeholder="Masukan deskripsi barang"
                    className="col-span-2 border-slate-300 border-2 rounded-md px-2"
                  />
                  <div></div>
                  <div>{params.touched.deskripsi && params.errors.deskripsi}</div>
                </div>

                <div className="w-full grid grid-cols-3 text-lg">
                  <label>Kategori Barang :</label>
                  <select
                    name="category"
                    onChange={(e) => {
                      params.setFieldValue("category", e.target.value);
                    }}
                    onBlur={() => {
                      params.setFieldTouched("category");
                    }}
                    className="col-span-2 border-slate-300 border-2 rounded-md px-2"
                  >
                    <option disabled selected>
                      Pilih Kategori
                    </option>
                    <option value="teknologi">Teknologi</option>
                    <option value="makanan">Makanan</option>
                    <option value="minuman">Minuman</option>
                    <option value="lainnya">Lainnya</option>
                  </select>
                  <div></div>
                  <div>{params.touched.category && params.errors.category}</div>
                </div>

                <div className="w-full grid grid-cols-3 text-lg">
                  <label>Image :</label>
                  <input
                    type="text"
                    name="image_url"
                    onChange={params.handleChange}
                    value={params.values.image_url}
                    onBlur={params.handleBlur}
                    placeholder="Masukan link gambar barang"
                    className="col-span-2 border-slate-300 border-2 rounded-md px-2"
                  />
                  <div></div>
                  <div>{params.touched.image_url && params.errors.image_url}</div>
                </div>

                <div className="w-full rounded-xl border border-slate-300 text-center mx-auto hover:bg-slate-500 py-2">
                  <button type="submit" disabled={loading} onClick={params.handleSubmit} className="item-center">
                    {loading === true ? "Loading..." : "Update"}
                  </button>
                </div>
              </form>
            );
          }}
        </Formik>
      </div>
    </div>
  );
}

export default EditForm;
