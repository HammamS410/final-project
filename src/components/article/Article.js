import React, { useState } from "react";
import "./articleContainer.css";

function Article(props) {
  return (
    <>
      <section className="w-64 h-full border-2 border-slate-300 rounded-xl shadow-md overflow-hidden">
        <div className="cursor-pointer">
          <img src={props.article.image_url} alt="" className="w-auto mb-4 px-1 py-1 rounded-xl" />

          <div className="">
            <h2 className="text-slate-800 font-bold text-xl text-center">{props.article.nama}</h2>
            {props.article.is_diskon == true ? (
              <p className="text-slate-500  text-center text-xs">
                Rp. {props.article.harga_display}
                <p className="text-slate-700 font-semibold text-2xl">Rp. {props.article.harga_diskon_display}</p>
              </p>
            ) : (
              <p className="text-2xl font-semibold text-center">Rp. {props.article.harga_display}</p>
            )}

            <p className="ml-2 text-md text-center">Stock : {props.article.stock}</p>
          </div>
        </div>
      </section>
    </>
  );
}

export default Article;
