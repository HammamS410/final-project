import axios from "axios";
import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import Navbar from "../Navbar";
import "./table.css";

function TableArticle() {
  const [articles, setArticles] = useState([""]);
  const [loading, setLoading] = useState(false);
  const [displayData, setDisplayData] = useState([]);

  const fetchArticles = async () => {
    setLoading(true);
    try {
      const response = await axios.get("https://arhandev.xyz/public/api/final/products", { headers: { Authorization: `Bearer ${localStorage.getItem("token")}` } });
      console.log(response.data);
      setArticles(response.data.data);
      setDisplayData(response.data.data);
    } catch (error) {
      console.log(error);
    } finally {
      setLoading(false);
    }
  };

  const onDelete = async (articleId) => {
    try {
      const response = await axios.delete(`https://arhandev.xyz/public/api/final/products/${articleId}`, { headers: { Authorization: `Bearer ${localStorage.getItem("token")}` } });
      alert("Item di delete");
      fetchArticles();
    } catch (error) {
      console.log(error);
      alert("Delete Gagal");
    }
  };

  useEffect(() => {
    fetchArticles();
  }, []);

  return (
    <div>
      <Navbar />
      <div className="mt-5">
        <div className="max-w-5xl mx-auto flex justify-between">
          <h2 className="item-center text-2xl font-bold">Setting Product</h2>
          <Link to="/create">
            <button className="border px-4 py-2 rounded-xl bg-slate-400 mb-3">Buat Product</button>
          </Link>
        </div>

        <div className="max-w-5xl mx-auto flex justify-end gap-4 mb-3">
          <select name="kategori">
            <option disabled selected>
              Pilih Kategori
            </option>
            <option value="teknologi">Teknologi</option>
            <option value="makanan">Makanan</option>
            <option value="minuman">Minuman</option>
            <option value="lainnya">Lainnya</option>
          </select>

          <input type="text" className="border-slate-300 border-2 rounded-md px-2" placeholder="Search..." />

          <button className="border px-3 py-1 rounded-md bg-slate-400">Filter</button>
          <button className="border px-3 py-1 rounded-md bg-slate-400">Reset</button>
        </div>

        <div>
          <table className="max-w-6xl mx-auto border-collapse">
            <tr>
              <th>Nama Barang</th>
              <th>Harga</th>
              <th>Harga Diskon</th>
              <th>Image</th>
              <th>Stock</th>
              <th>Category</th>
              <th>Dibuat Pada</th>
              <th>Diupdate Pada</th>
              <th>Action</th>
            </tr>
            <tbody>
              {displayData.map((article) => {
                return (
                  <tr>
                    <td>{article.nama} </td>
                    <td>{article.harga_display} </td>
                    <td>{article.harga_diskon_display}</td>
                    <td className="">
                      <img src={article.image_url} alt="" className="w-20 mx-auto" />
                    </td>
                    <td>{article.stock}</td>
                    <td>{article.category}</td>
                    <td>{article.created_at}</td>
                    <td>{article.updated_at}</td>
                    <td>
                      <div className="flex gap-2">
                        <Link to={`/edit/${article.id}`}>
                          <button className="px-5 py-2 text-white font-bold rounded-lg bg-yellow-600">Edit</button>
                        </Link>
                        <button
                          onClick={() => {
                            onDelete(article.id);
                          }}
                          className="px-5 py-2 text-white font-bold rounded-lg bg-red-600"
                        >
                          Delete
                        </button>
                      </div>
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
}

export default TableArticle;
