import { Formik } from "formik";
import React, { useEffect, useState } from "react";
import * as Yup from "yup";
import axios from "axios";
import Navbar from "../Navbar";
import "./register.css";
import { useNavigate } from "react-router-dom";

const validationSchema = Yup.object({
  email: Yup.string().required().email("email tidak valid"),
  password: Yup.string().required(),
});

function LoginForm() {
  const navigate = useNavigate();
  const [loading, setLoading] = useState(false);
  const initialState = {
    email: "",
    password: "",
  };

  const handleSubmit = async (values) => {
    setLoading(true);
    try {
      const response = await axios.post("https://arhandev.xyz/public/api/final/login", {
        email: values.email,
        password: values.password,
      });
      localStorage.setItem("token", response.data.data.token);
      localStorage.setItem("username", response.data.data.user.username);
      console.log(response.data.data);
      alert("Login Berhasil");
      navigate("/home");
    } catch (error) {
      console.log(error);
      alert(error.response.data.info);
    } finally {
      setLoading(false);
    }
  };

  return (
    <div>
      <Navbar />
      <div>
        <Formik initialValues={initialState} onSubmit={handleSubmit} validationSchema={validationSchema}>
          {(params) => {
            return (
              <form className="reg-form w-2/5 mx-auto flex flex-col gap-4 my-8 items-center border-2 border-slate-300 rounded-2xl px-4 py-4">
                <h1 className="text-2xl font-bold">Login</h1>

                <div className="w-full grid grid-cols-3 text-lg">
                  <label>Email :</label>
                  <input type="email" name="email" onChange={params.handleChange} value={params.values.email} onBlur={params.handleBlur} placeholder=" Masukan email" className="col-span-2 border-slate-300 border-2 rounded-md pl-2" />
                  <div></div>
                  <div>{params.touched.email && params.errors.email}</div>
                </div>

                <div className="w-full grid grid-cols-3 text-lg">
                  <label>Password :</label>
                  <input
                    type="password"
                    name="password"
                    onChange={params.handleChange}
                    value={params.values.password}
                    onBlur={params.handleBlur}
                    placeholder=" Masukan password"
                    className="col-span-2 border-slate-300 border-2 rounded-md pl-2"
                  />
                  <div></div>
                  <div>{params.touched.password && params.errors.password}</div>
                </div>

                <div className="w-full rounded-full border border-slate-500 text-center mx-auto bg-slate-700 text-white hover:bg-slate-400 py-2">
                  <button type="submit" disabled={loading} onClick={params.handleSubmit} className="item-center">
                    {loading === true ? "Loading..." : "Login"}
                  </button>
                </div>
              </form>
            );
          }}
        </Formik>
      </div>
    </div>
  );
}

export default LoginForm;
