import axios from "axios";
import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import Article from "../article/Article";
import Navbar from "../Navbar";
import "./articleContainer.css";

function ArticleContainer() {
  const [articles, setArticles] = useState([]);
  const [loading, setLoading] = useState(false);

  const fetchArticles = async () => {
    setLoading(true);
    try {
      const response = await axios.get("https://arhandev.xyz/public/api/final/products");
      console.log(response.data);
      setArticles(response.data.data);
    } catch (error) {
      console.log(error);
    } finally {
      setLoading(false);
    }
  };

  useEffect(() => {
    fetchArticles();
  }, []);
  return loading ? (
    <div className="text-3xl font-bold text-center text-slate-600 translate-y-32">Loading...</div>
  ) : (
    <>
      <Navbar />
      <div className="container-button">
        <div className="flex justify-between max-w-7xl mx-auto items-center mt-5">
          <p className="text-3xl font-bold">Product List</p>
          <Link to="/create">
            <button className="border-2 border-slate-500 py-1.5 px-2 rounded-lg bg-slate-400 text-white" type="submit">
              Create Product
            </button>
          </Link>
        </div>
        <div className="container">
          {articles.map((article) => {
            return <Article article={article} />;
          })}
        </div>
      </div>
    </>
  );
}

export default ArticleContainer;
