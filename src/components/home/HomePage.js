import axios from "axios";
import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import Article from "../article/Article";
import Navbar from "../Navbar";
import "./home.css";

function HomePage() {
  const [articles, setArticles] = useState([]);
  const [loading, setLoading] = useState(false);

  const fetchArticles = async () => {
    setLoading(true);
    try {
      const response = await axios.get("https://arhandev.xyz/public/api/final/products");
      console.log(response.data);
      setArticles(response.data.data);
    } catch (error) {
      console.log(error);
    } finally {
      setLoading(false);
    }
  };

  useEffect(() => {
    fetchArticles();
  }, []);

  return loading ? (
    <div className="text-3xl font-bold text-center text-slate-600 translate-y-32">Loading...</div>
  ) : (
    <>
      <div>
        <Navbar />
        <div>
          <div className="relative">
            <img src="https://mdbootstrap.com/img/Photos/Slides/img%20(22).jpg" className="mx-auto w-2/2 mt-5" alt="..." />
            <div className="flex items-center justify-evenly mt-5">
              <p className="text-2xl font-semibold ">CATALOG PRODUCT</p>
              <Link to="/product">
                <button className="border-2 px-8 py-1 border-slate-700 rounded-full hover:bg-slate-700 hover:text-white">See More</button>
              </Link>
            </div>
          </div>

          <ul onClick="">
            <div className="container">
              {articles.map((article) => {
                return <Article article={article} />;
              })}
            </div>
          </ul>
        </div>
      </div>
    </>
  );
}

export default HomePage;
